<?php
//osztály betöltése
include "class.Address.inc";

echo '<h2>Üres objektum létrehozása</h2>';
$address = new Address;

echo '<pre>'.var_export($address,true).'</pre>';

echo '<h2>Objektum tulajdonságok feltöltése adatokkal</h2>';
$address->address_line_1 = 'Teszt utca 1.';
$address->city_name = 'Szentendre';
$address->country_name = 'Magyarország';
$address->postal_code = 2000;
//$address->valami = "hacked";
echo '<pre>'.var_export($address,true).'</pre>';

echo '<h2>Objektum eljárás hívása a cím kiírására</h2>';
echo $address->display();
echo '<h2>Objektum tulajdonság kiírása</h2>';
echo $address->country_name;
echo '<h2>Objektum védett tulajdonság tesztelése - (protected)</h2>';
echo $address->postal_code;

echo '<h2>Objektum készítése asszociativ tömbből a konstruktor használatával</h2>';
$data = [
    'address_line_1'=>'teszt utca 2.',
    'city_name' => 'Szentendre',
    'country_name' => 'Magyarország',
    'postal_code' => 5555,
];
$address_2 = new Address($data);
echo '<pre>'.var_export($address_2,true).'</pre>';
echo $address_2->display();