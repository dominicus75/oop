<?php

/**
 * Címkezelő osztály
 */
class Address {
    //cimsor 1public $postal_code;
    public $address_line_1;
    //cimsor 2
    public $address_line_2;
    //irsz
    protected $_postal_code;
    //ország
    public $country_name;
    //város
    public $city_name;
    //városrész
    public $subdivision_name;
    
    //cím azonosító
    protected $_address_id;
    
    //timestamps
    protected $_time_created = 11243;
    protected $_time_updated = 67890;
    
    /**
     * Konstruktor, akkor fut, amikor példány készül (new)
     */
    public function __construct($data=[]) {
        $this->_time_created = time();
        
        if(is_array($data)){//ha kaptunk adatokat és az tömbb, megpróbáljuik felépíteni az objektumot,  he nem jó vmi, trigger error
            foreach($data as $property => $value){
                $this->$property = $value;
            }
        }else{
            trigger_error('A kapott adatokból nem építhető fel az ojjetum');
        }
    }
    
    /**
     * getter - akkor fut, amikor nem létező vagy védett tulajdonságot próbálunk kiolvasni
     * @param string $name
     * @return mixed
     */
    public function __get($name){
        //ha nem lenne irsz, keressünk...
       if(!$this->_postal_code){
           $this->_postal_code = $this->_postal_code_search();
       }
        
       $protected_property_name = '_'.$name;//ha van védett tul, akkor az elnevézi konvenció miatt így lenne nevezve
       if(property_exists($this, $protected_property_name)){
           return $this->$protected_property_name;
       }
        
        trigger_error("Nem létező vagy védett tulajdonságot próbálsz elérni (__get):  $name");
    }
    
    /**
     * setter - akkor fut, amikor nem létező vagy védett tulajdonságot próbálunk beállítani
     * @param string $name
     * @param mixed $value
     * @return void
     */
    public function __set($name, $value) {
        //kivétel megadása hogy 'kivülről' lehessen felvinni eddig nem létező postal_code tulajdonságot
        if($name == 'postal_code'){
            $this->$name = $value;
            return;
        }
        trigger_error("Nem létező vagy védett tulajdonságot próbálsz elérni (__set): $name | $value");
    }
    
    /**
     * Irsz keresés város és városrész alapján
     * @todo: write the database logic
     * @return string
     */
    protected function _postal_code_search(){
        return 'keresés...';
    }
    /**
     * Címpéldány 'kiírása'
     * @return string
     */
    public function display(){
        //cimsor 1
        $output = $this->address_line_1;
        //ha van , címsor 2
        if($this->address_line_2){
            $output .= '<br>'.$this->address_line_2;
        }
        //város, városrész ha van
        $output .= '<br>'.$this->city_name;
        $output .= $this->subdivision_name ? ", $this->subdivision_name" : "";
        //ország
        $output .= '<br>'.$this->country_name;
        //irsz
        $output .= '<br>'. $this->postal_code;
        
        return $output;
    }
    
}